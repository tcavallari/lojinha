package edu.ifsp.lojinha.web;

import edu.ifsp.lojinha.modelo.Usuario;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class SessionListener
 *
 */
@WebListener
public class SessionListener implements HttpSessionListener {


    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
    	Usuario usuario = (Usuario)se.getSession().getAttribute("usuario");
    	System.out.println("Fim de sessao. Usuario: " + usuario.getUsername());
    }
	
}
